class Observer {
    constructor(data) {
        this.walk(data)
    }
    walk(data) {
        // 1.判断data是否是对象
        if(!data || typeof data !== 'object'){
            return
        }
        // 2.遍历data对象的所有属性
        Object.keys(data).forEach(key => {
            this.defineReactive(data, key, data[key])
        })
    }
    defineReactive(obj, key, val) {
        let that = this
        // 负责手机依赖 并发送通知
        let dep = new Dep()
        // 如果val是对象则会将val内部属性转换为响应数据
        that.walk(val)
        Object.defineProperty(obj, key,{
            enumerable:true,
            configurable:true,
            get() {
                Dep.target && dep.addSub(Dep.target)
                return val
            },
            set(newValue) {
                if(newValue === val) {
                    return
                }
                val = newValue
                // 
                that.walk(newValue)
                // 发送通知
                dep.notify()
            }
        })
    }
}